package logic.dao;

import org.hibernate.Session;
import logic.entity.Student;

import java.util.ArrayList;

public interface StudentDao {
    ArrayList<Student> getStudentList(Session session);
}
