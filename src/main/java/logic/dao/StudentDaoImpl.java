package logic.dao;

import org.hibernate.Session;
import logic.entity.Student;

import java.util.ArrayList;

public class StudentDaoImpl implements StudentDao {
    @Override
    public ArrayList<Student> getStudentList(Session session) {
        return (ArrayList<Student>) session.createQuery("from Student").list();
    }
}
