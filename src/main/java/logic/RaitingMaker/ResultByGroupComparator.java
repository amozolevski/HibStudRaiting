package logic.RaitingMaker;

import java.util.Comparator;

public class ResultByGroupComparator implements Comparator<GroupResults> {
    @Override
    public int compare(GroupResults o1, GroupResults o2) {
        if(o1.getCommonRaiting() < o2.getCommonRaiting())
            return 1;

        if(o1.getCommonRaiting() > o2.getCommonRaiting())
            return -1;
        return 0;
    }
}
