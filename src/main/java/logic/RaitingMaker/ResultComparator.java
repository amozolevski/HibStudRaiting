package logic.RaitingMaker;

import logic.ResultCalculator.StudentResults;

import java.util.Comparator;

public class ResultComparator implements Comparator<StudentResults> {
    @Override
    public int compare(StudentResults o1, StudentResults o2) {

        if(o1.getExamSumResult() < o2.getExamSumResult())
            return 1;

        if(o1.getExamSumResult() > o2.getExamSumResult())
            return -1;

        return 0;
    }
}
