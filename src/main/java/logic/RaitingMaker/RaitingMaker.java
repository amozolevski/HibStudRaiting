package logic.RaitingMaker;

import logic.ResultCalculator.StudentResults;

import java.util.ArrayList;
import java.util.List;

public class RaitingMaker {

    public static void currentRaiting(ArrayList<StudentResults> studentResults){
        studentResults.stream()
                        .sorted(new ResultComparator())
                        .forEach(System.out :: println);
    }

    public static void currentRaitingByGroups(ArrayList<StudentResults> studentResults){
        studentResults.stream()
                        .sorted(new GroupComparator().thenComparing(new ResultComparator()))
                        .forEach(System.out :: println);
    }

    public static void currentRaitingThreeStud(ArrayList<StudentResults> studentResults){
        studentResults.stream()
                        .sorted((s1, s2) -> s1.getSurname().compareTo(s2.getSurname()))
                        .limit(3)
                        .forEach(System.out :: println);
    }

    public static void currentRaitingThreeBestStud(ArrayList<StudentResults> studentResults){
        studentResults.stream()
                .sorted(new ResultComparator())
                .limit(3)
                .forEach(System.out :: println);
    }

    public static void currentRaitingGroups(ArrayList<StudentResults> studentResults){
        List<Double> groupAlist = new ArrayList<>();
        double resA = 0.0;

        List<Double> groupBlist = new ArrayList<>();
        double resB = 0.0;

        List<Double> groupClist = new ArrayList<>();
        double resC = 0.0;

        for (StudentResults sr : studentResults) {

            if (sr.getGroupName().equalsIgnoreCase("A")){
                groupAlist.add(sr.getExamSumResult());
                resA += sr.getExamSumResult();
            }

            if (sr.getGroupName().equalsIgnoreCase("B")){
                groupBlist.add(sr.getExamSumResult());
                resB += sr.getExamSumResult();
            }

            if (sr.getGroupName().equalsIgnoreCase("C")){
                groupClist.add(sr.getExamSumResult());
                resC += sr.getExamSumResult();
            }
        }

        List<GroupResults> groupResultsList = new ArrayList<>();
        groupResultsList.add(new GroupResults("A", resA/groupAlist.size()));
        groupResultsList.add(new GroupResults("B", resB/groupBlist.size()));
        groupResultsList.add(new GroupResults("C", resC/groupClist.size()));

        groupResultsList.stream()
                        .sorted(new ResultByGroupComparator())
                        .forEach(System.out :: println);
    }

    public static void currentRaitingResultLessSixty(ArrayList<StudentResults> studentResults){
        studentResults.stream()
                .filter((st) -> st.getExamSumResult() < 60)
                .sorted(new ResultComparator())
                .forEach(System.out :: println);
    }

}