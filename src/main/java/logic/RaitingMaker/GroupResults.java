package logic.RaitingMaker;

public class GroupResults {

    private String groupName;
    private double commonRaiting;

    public GroupResults(String groupName, double commonRaiting) {
        this.groupName = groupName;
        this.commonRaiting = commonRaiting;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public double getCommonRaiting() {
        return commonRaiting;
    }

    public void setCommonRaiting(double commonRaiting) {
        this.commonRaiting = commonRaiting;
    }

    @Override
    public String toString() {
        return "groupName = " + groupName +
                ", commonRaiting = " + commonRaiting;
    }
}
