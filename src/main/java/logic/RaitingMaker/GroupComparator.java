package logic.RaitingMaker;

import logic.ResultCalculator.StudentResults;

import java.util.Comparator;

public class GroupComparator implements Comparator<StudentResults> {

    @Override
    public int compare(StudentResults o1, StudentResults o2) {
        return o1.getGroupName().compareTo(o2.getGroupName());
    }
}
