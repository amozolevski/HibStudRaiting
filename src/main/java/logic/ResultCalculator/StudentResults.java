package logic.ResultCalculator;

import java.io.Serializable;

public class StudentResults implements Serializable {

    private String name;
    private String surname;
    private String groupName;
    private double examSumResult;

    public StudentResults(String name, String surname, String groupName, double examSumResult) {
        this.name = name;
        this.surname = surname;
        this.groupName = groupName;
        this.examSumResult = examSumResult;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public double getExamSumResult() {
        return examSumResult;
    }

    public void setExamSumResult(double examSumResult) {
        this.examSumResult = examSumResult;
    }

    @Override
    public String toString() {
        return "Student: " + name + ' ' + surname +
                ", groupName = " + groupName +
                ", examSumResult = " + examSumResult;
    }
}
