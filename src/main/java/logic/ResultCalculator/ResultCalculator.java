package logic.ResultCalculator;

import logic.entity.Results;
import logic.entity.Student;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

public class ResultCalculator{

    private ArrayList<StudentResults> studentResultsList = new ArrayList<>();

    public ArrayList<StudentResults> getStudentResultsList() {
        return studentResultsList;
    }

    public void calculateCommonResult(ArrayList<Student> studentsList){

        double cofactorSum = 0.0;
        double subjectRes = 0.0;

//        Рейтинг расчитывается по формуле п1*к1+п2*к2+п3*к3+п4*к4/(к1+к2+к3+к4)
        for (Student st : studentsList) {
            for (Results r : st.getResultsSet()) {
                subjectRes = subjectRes + r.getExamCofactor() * (r.getExamResult() == null ? 0 : r.getExamResult());
                cofactorSum += r.getExamCofactor();
            }

            //округляю до 2-х после точки, сорри за русский
            double result = new BigDecimal(subjectRes/cofactorSum).setScale(2, RoundingMode.HALF_UP).doubleValue();
            studentResultsList.add(new StudentResults(st.getName(), st.getSurname(), st.getGroupName(), result));
        }

    }
}
