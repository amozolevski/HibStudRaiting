package logic.entity;


import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "results")
public class Results implements Serializable{

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "pkey_exam", insertable = false, updatable = false)
    private Exam exam;

    @Id
    @Column(name = "pkey")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int ID;

    @ManyToOne
    @JoinColumn(name = "pkey_student", nullable = false, insertable = false)
    private Student student;

    @Column(name = "pkey_exam")
    private int examID;

    @Column(name = "value")
    private Integer examResult;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getExamID() {
        return examID;
    }

    public void setExamID(int examID) {
        this.examID = examID;
    }

    public Integer getExamResult() {
        return examResult;
    }

    public void setExamResult(Integer examResult) {
        this.examResult = (examResult == null) ? 0 : examResult;
    }

    public Double getExamCofactor(){
        return exam.getCofactor();
    }

    @Override
    public String toString() {
        return "examID = " + exam.getID() +
                ", examResult = " + getExamResult() +
                ", examCofactor = " + getExamCofactor();
    }
}
