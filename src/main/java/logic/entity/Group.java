package logic.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "groups")
public class Group implements Serializable {

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "pkey")
    private Student student;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pkey")
    private int ID;

    @Column(name = "name")
    private String groupName;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    @Override
    public String toString() {
        return "GroupID = " + ID + ", groupName= " + groupName;
    }
}
