package logic.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "exam")
public class Exam implements Serializable {

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "pkey")
    private Results results;

    @Id
    @Column(name = "pkey")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int ID;

    @Column(name = "cofactor")
    private Double cofactor;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public Double getCofactor() {
        return cofactor;
    }

    public void setCofactor(Double cofactor) {
        this.cofactor = cofactor;
    }

    @Override
    public String toString() {
        return "ExamID = " + ID +
                ", cofactor = " + cofactor;
    }
}
