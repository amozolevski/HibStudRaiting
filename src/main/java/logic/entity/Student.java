package logic.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "students")
public class Student implements Serializable{

    @Id
    @Column(name = "pkey")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int ID;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "surname", nullable = false)
    private String surname;

    @Column(name = "pkey_group", nullable = false)
    private int groupID;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "pkey_group", updatable = false, insertable = false)
    private Group group;

    @OneToMany(mappedBy = "student")
    private Set<Results> resultsSet;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getGroupID() {
        return groupID;
    }

    public void setGroupID(int groupID) {
        this.groupID = groupID;
    }

    public Set<Results> getResultsSet() {
        return resultsSet;
    }

    public void setResultsSet(Set<Results> resultsSet) {
        this.resultsSet = resultsSet;
    }

    public String getGroupName(){
        return group.getGroupName();
    }

    @Override
    public String toString() {
        return "Student: " + getName() +
                " " + getSurname() +
                ", group " + getGroupName() +
                ", exams result: " + getResultsSet();
    }
}
