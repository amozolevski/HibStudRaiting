/*Сформировать рейтинг студентов Имя фамилия значение рейтинга.  */

package logic;

import logic.RaitingMaker.RaitingMaker;
import logic.ResultCalculator.ResultCalculator;
import org.hibernate.Session;
import logic.dao.StudentDaoImpl;
import logic.entity.Student;
import logic.persistence.HibernateUtil;

import java.util.ArrayList;

public class App {
    public static void main(String[] args) {

        Session session = HibernateUtil.getSessionFactory().openSession();

        StudentDaoImpl studentDao = new StudentDaoImpl();
        ArrayList<Student> studentList = studentDao.getStudentList(session);

        ResultCalculator resultCalculator = new ResultCalculator();
        resultCalculator.calculateCommonResult(studentList);

        //Рейтинг по потоку
//        RaitingMaker.currentRaiting(resultCalculator.getStudentResultsList());

        //Рейтинги по группам
//        RaitingMaker.currentRaitingByGroups(resultCalculator.getStudentResultsList());

        //Имя Фамилия первых 3-х студентов на потоке.
//        RaitingMaker.currentRaitingThreeStud(resultCalculator.getStudentResultsList());

        //Имя Фамилия 3-х лучших студентов по группам.
//        RaitingMaker.currentRaitingThreeBestStud(resultCalculator.getStudentResultsList());

        //Получить рейтинг групп.
        RaitingMaker.currentRaitingGroups(resultCalculator.getStudentResultsList());

        //Получить список студентов рейтинг которых <60.
//        RaitingMaker.currentRaitingResultLessSixty(resultCalculator.getStudentResultsList());

        HibernateUtil.shutDownSession();
    }
}
